const files = require("fs");

const WORD_FILE = __dirname+"/../assets/words.txt";



const getRandomWordAndCategory = () => {
  let words_array = readWordsFile(WORD_FILE);
  return getRandomWordAndCategoryFromFile(words_array);
};

const getRandomWordAndCategoryFromFile = words_array => {

  var wordAndCategory = words_array[Math.floor(Math.random() * words_array.length)].split("#");
  var word = wordAndCategory[0];
  var category = wordAndCategory[1];	
	
  return {
	  word : word,
	  category: category
  };
};

const getRandomWord = () => {
  let words_array = readWordsFile(WORD_FILE);
  return getRandomWordFromFile(words_array);
};


const getRandomWordFromFile = words_array => {
  var wordAndCategory = words_array[Math.floor(Math.random() * words_array.length)].split("#");
  var word = wordAndCategory[0];	
  return word;
};

const readWordsFile = filename => {
  const words_array = files.readFileSync(filename, "utf-8").split("\n").filter(str => str!='');
  return words_array;
};

module.exports = {
  WORD_FILE,
  readWordsFile,
  getRandomWordAndCategoryFromFile,
  getRandomWordFromFile,
  getRandomWordAndCategory,
  getRandomWord
};
