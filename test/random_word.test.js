const RandomWord = require("../lib/random_word");

describe("The RandomWord module is able to", () => {
  test("Read a file and return an array with an item for each line", () => {
    expect(RandomWord.readWordsFile(RandomWord.WORD_FILE)).toHaveLength(8);
  });

  test("get a random word that is never null", () => {
    const words = ["bagagem", "adjudicar", "ribeira"];
    expect(words).toContain(RandomWord.getRandomWordFromFile(words));
  });
  
  test("get a random word and category that is never null", () => {
    const fileItems = ["cadeira#mobiliario", "comboio#transporte", "travesseiro#objeto"];
	const categories = ["mobiliario", "transporte", "objeto"];

    expect(categories).toContain(RandomWord.getRandomWordAndCategoryFromFile(fileItems).category);
  });

});
